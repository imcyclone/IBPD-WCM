package com.ibpd.shopping.assist;

import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONArray;

import com.ibpd.shopping.entity.ProductAttrValueEntity;

public class ExtProductAttrEntity {

	private List<ProductAttrValueEntity> attrList=new ArrayList<ProductAttrValueEntity>(); 
	private Long id;
	private Long attrId;
	private String attrName="";
	private String attrValue="";
	private Integer order=0;
	private Long productId=-1L;
	public Long getAttrId() {
		return attrId;
	}
	public void setAttrId(Long attrId) {
		this.attrId = attrId;
	}
	public String getAttrName() {
		return attrName;
	}
	public void setAttrName(String attrName) {
		this.attrName = attrName;
	}
	public String getAttrValue() {
		return attrValue;
	}
	public void setAttrValue(String attrValue) {
		this.attrValue = attrValue;
	}
	public Integer getOrder() {
		return order;
	}
	public void setOrder(Integer order) {
		this.order = order;
	}
	public void setAttrList(List<ProductAttrValueEntity> attrList) {
		this.attrList = attrList;
	}
	public List<ProductAttrValueEntity> getAttrList() {
		return attrList;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getId() {
		return id;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public Long getProductId() {
		return productId;
	}
	@Override
	public String toString() {
		JSONArray j=JSONArray.fromObject(this);
		return j.toString().substring(1,j.toString().length()-1);
	}

}
