package com.ibpd.shopping.assist;

import java.lang.reflect.InvocationTargetException;

import com.ibpd.shopping.entity.MallHomePageEntity;

public class ExtMallHomePageEntity extends MallHomePageEntity {

	private String productName="";
	private Float price=0f;
	private String image="";
	public ExtMallHomePageEntity(MallHomePageEntity mall){
		try {
			InterfaceUtil.swap(mall, this);
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getProductName() {
		return productName;
	}
	public void setPrice(Float price) {
		this.price = price;
	}
	public Float getPrice() {
		return price;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getImage() {
		return image;
	}
}
