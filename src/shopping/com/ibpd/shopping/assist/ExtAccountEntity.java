package com.ibpd.shopping.assist;

import java.lang.reflect.InvocationTargetException;

import com.ibpd.shopping.entity.AccountEntity;

public class ExtAccountEntity extends AccountEntity {
	/**
	 * 收藏的商品个数
	 */
	private Integer favoriteProductCount=0;
	/**
	 * 收藏的店铺个数
	 */
	private Integer favoriteTenantCount=0;
	public ExtAccountEntity(AccountEntity acc){
		try {
			InterfaceUtil.swap(acc, this);
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void setFavoriteProductCount(Integer favoriteProductCount) {
		this.favoriteProductCount = favoriteProductCount;
	}
	public Integer getFavoriteProductCount() {
		return favoriteProductCount;
	}
	public void setFavoriteTenantCount(Integer favoriteTenantCount) {
		this.favoriteTenantCount = favoriteTenantCount;
	}
	public Integer getFavoriteTenantCount() {
		return favoriteTenantCount;
	}
}
