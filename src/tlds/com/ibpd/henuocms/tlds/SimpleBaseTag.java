package com.ibpd.henuocms.tlds;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.hsqldb.lib.StringUtil;

import com.ibpd.henuocms.web.controller.manage.SubSite;

public class SimpleBaseTag extends SimpleTagSupport {

	private Object items=null;
    public Object getItems() {
		return items;
	}
	public void setItems(Object items) {
		this.items = items;
	}
	private Iterator ite = null;
	protected void init(){
        
		if(items==null)
			return;
        Object tempItem = items;
        // 如果是集合
        if (tempItem instanceof Collection) {
            setIte(((Collection) tempItem).iterator());
        }
        // 如果是数组
        else if (tempItem instanceof Object[]) {
            setIte(Arrays.asList((Object[]) tempItem).iterator());
        }
     // 如果是list
        else if (tempItem instanceof List) {
            setIte(((List)tempItem).iterator());
        }else {
            throw new RuntimeException("不能转换:"+items.getClass().getName());
        }
	}
	public void setIte(Iterator ite) {
		this.ite = ite;
	}
	public Iterator getIte() {
		return ite;
	}
	
	protected HttpServletRequest getRequest(){
		HttpServletRequest request = (HttpServletRequest)((PageContext)this.getJspContext()).getRequest();
		return request;
	}
	protected Boolean checkViewerMode(){
		if(getRequest().getParameter("type")!=null && getRequest().getParameter("type").equals(SubSite.WEBPAGE_TYPE_VIEWER)){
			return true;
		}else{
			return false;
		}
	}
	protected String escapeValue(String str,String escape){
		if(StringUtil.isEmpty(escape)){
//			return str;
		}else{
			String[] es=escape.split(";");
			for(String e:es){
				String[] t=e.split(":");
				if(t.length==2)
					str=str.replace(t[0], t[1]);
			}
		}
		if(getRequest().getParameter("type")!=null && getRequest().getParameter("type").equals(SubSite.WEBPAGE_TYPE_VIEWER)){
			return str;
		}else
			return str.replace(getRequest().getContextPath(), "");
	}

}
