package com.ibpd.henuocms.tlds;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;

import org.apache.commons.lang.StringUtils;
import org.hsqldb.lib.StringUtil;

import com.ibpd.dao.impl.ServiceProxyFactory;
import com.ibpd.henuocms.assist.ExtContentEntity;
import com.ibpd.henuocms.common.IbpdCommon;
import com.ibpd.henuocms.common.ListSortUtil;
import com.ibpd.henuocms.entity.ContentEntity;
import com.ibpd.henuocms.entity.NodeEntity;
import com.ibpd.henuocms.entity.ext.NodeExtEntity;
import com.ibpd.henuocms.service.content.ContentServiceImpl;
import com.ibpd.henuocms.service.content.IContentService;
import com.ibpd.henuocms.service.node.INodeService;
import com.ibpd.henuocms.service.node.NodeServiceImpl;

public class ContentEachTag extends SimpleBaseTag{
	public static String defaultNodeId="OWNER";
	private Object nodeId=defaultNodeId;
	private String var="content";
	private String orderField="order";
	private String orderType="asc";
	private String siteId="currentSite";
	private String startPos="0";
	private String itemCount="10";
	private String itemFilter="";
	private Integer currentStartPos=0;
	private Integer currentItemCount=10;
	@Override
	public void doTag() throws JspException, IOException {
		_init();
		init();
		if(getIte()==null){
            return ;
            }
	       while (getIte().hasNext()) {
	            Object obj = getIte().next();
	            getJspContext().setAttribute(var, obj);
	            //输出标签体
	            if(getJspBody()!=null)
	            	getJspBody().invoke(null);
	        }
	}
	private void _init(){
		try{
			if(StringUtil.isEmpty(var)){
				var="content";
			}
			if(StringUtil.isEmpty(orderField)){
				orderField="order";
			}
			if(StringUtil.isEmpty(orderType)){
				orderType="asc";
			}else{
				if(!orderType.toLowerCase().trim().equals("asc") && !orderType.toLowerCase().trim().equals("desc")){
					orderType="asc";
				}
			}
			if(StringUtils.isNumeric(startPos)){
				currentStartPos=Integer.parseInt(startPos);
			}else{
				currentStartPos=0;
			}
			if(StringUtils.isNumeric(itemCount)){
				currentItemCount=Integer.parseInt(itemCount);
			}else{
				currentItemCount=10;
			}
				Long currentNodeId=-1L;
				this.setNodeId(this.getNodeId()==null?defaultNodeId:this.getNodeId());
				if(this.getNodeId().toString().trim().toUpperCase().equals(this.defaultNodeId)){
					String tmp=getRequest().getParameter("nodeId");
					if(!StringUtil.isEmpty(tmp)){
						if(StringUtils.isNumeric(tmp)){
							currentNodeId=Long.parseLong(tmp);
						}
					}
				}else{
					if(StringUtils.isNumeric(this.getNodeId().toString())){
						currentNodeId=Long.parseLong(this.getNodeId().toString());
					}
				}
				//上面代码的作用是获取当前传来的NodeId，可以是从地址栏传来的（nodeId参数的值为空即不传值或者为OWNER），也可以是
				//具体的ID
				//siteID有三种可能：1、地址栏传来的 2、设置的 3、从nodeId获取
				INodeService nodeService=(INodeService) ServiceProxyFactory.getServiceProxy(NodeServiceImpl.class);
				Long  currentSiteId=null;
				if(StringUtil.isEmpty(siteId))
					siteId="currentSite";
				if(siteId.trim().toUpperCase().equals("CURRENTSITE")){
					NodeEntity ne=nodeService.getEntityById(currentNodeId);
					if(ne!=null){
						currentSiteId=ne.getSubSiteId();
					}else{
						currentSiteId=null;
					}
				}else if(siteId.trim().toUpperCase().equals("OWNER")){
					String tmp=getRequest().getParameter("siteId");
					if(!StringUtil.isEmpty(tmp)){
						if(StringUtils.isNumeric(tmp)){
							currentSiteId=Long.parseLong(tmp);
						}
					}
				}else if(StringUtils.isNumeric(siteId)){
					currentSiteId=Long.parseLong(siteId);
				}
				IContentService contServ=(IContentService) ServiceProxyFactory.getServiceProxy(ContentServiceImpl.class);
			List<ExtContentEntity> nodeList=contServ.getContentList(currentSiteId,currentNodeId,currentItemCount,currentStartPos,getFilterFormat(itemFilter),orderField+" "+orderType);
			nodeList=orderList(nodeList);
			if(nodeList!=null && nodeList.size()>0){
				process(nodeList);
				setItems(nodeList);
				setIte(nodeList.iterator()); 
			}else{
				setItems(null);
				setIte(null);
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	private String getFilterFormat(String f){
		if(f==null)
			return "";
		if(StringUtils.isBlank(f)){
			return "";
		}
		return IbpdCommon.getInterface().getOtherQueryString(itemFilter);
	}
	private void process(List<ExtContentEntity> l){
		if(l==null || l.size()==0)
			return;
		for(ExtContentEntity c:l){
			String s="";
			String n="";
			if(c.getAttrEntity()!=null){
				if(!StringUtils.isBlank(c.getAttrEntity().getColor()) || !StringUtils.isBlank(c.getAttrEntity().getSize())){
					s+="<font size='"+c.getAttrEntity().getSize()+"' color='"+c.getAttrEntity().getColor()+"'>";
					n+="</font>";
				}
				if(c.getAttrEntity().getEm()){
					s+="<em>";
					n+="</em>";
				}
				if(c.getAttrEntity().getStrong()){
					s+="<strong>";
					n+="</strong>";
				}
				if(c.getAttrEntity().getU()){
					s+="<u>";
					n+="</u>";				
				}
				c.setTitle(s+c.getTitle()+n);	
			}
		}
	}
	private List<ExtContentEntity> orderList(List<ExtContentEntity> list){
		ListSortUtil<ExtContentEntity> sortList = new ListSortUtil<ExtContentEntity>(); 
		sortList.sort(list, orderField, orderType);
		return list;
	}
	public static String getDefaultNodeId() {
		return defaultNodeId;
	}
	public static void setDefaultNodeId(String defaultNodeId) {
		ContentEachTag.defaultNodeId = defaultNodeId;
	}
	public Object getNodeId() {
		return nodeId;
	}
	public void setNodeId(Object nodeId) {
		this.nodeId = nodeId;
	}
	public String getVar() {
		return var;
	}
	public void setVar(String var) {
		this.var = var;
	}
	public String getOrderField() {
		return orderField;
	}
	public void setOrderField(String orderField) {
		this.orderField = orderField;
	}
	public String getOrderType() {
		return orderType;
	}
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	public String getSiteId() {
		return siteId;
	}
	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}
	public void setStartPos(String startPos) {
		this.startPos = startPos;
	}
	public String getStartPos() {
		return startPos;
	}
	public void setItemCount(String itemCount) {
		this.itemCount = itemCount;
	}
	public String getItemCount() {
		return itemCount;
	}
	public void setItemFilter(String itemFilter) {
		this.itemFilter = itemFilter;
	}
	public String getItemFilter() {
		return itemFilter;
	}

}
