package com.ibpd.henuocms.tlds;
import javax.servlet.jsp.*;
import javax.servlet.jsp.tagext.*;

import com.ibpd.henuocms.common.IbpdLogger;

import java.util.Hashtable;
import java.util.List;
import java.io.Writer;
import java.io.IOException;

/**
 * 条件判断tag 可用
 * @author mg by qq:349070443
 *编辑于 2015-6-20 下午05:43:41
 */
public class IfTag extends BodyTagSupport
{
    private boolean value;
    private Object items;
    /**
     *设置属性的值。
     */
    public void setValue(boolean value)
    {
        this.value=value;
    }
    
    /**
     *doStartTag方法，如果value为true，那么
     *就计算tagbody的值，否则不计算body的值。
     */
    public int doStartTag() throws JspTagException
    {
        if(value)
        {
        	IbpdLogger.getLogger(this.getClass()).info("value is true");
           return EVAL_BODY_INCLUDE; 
         }  
         else
         { 
        	 IbpdLogger.getLogger(this.getClass()).info("value is false");
            return SKIP_BODY; 
         } 
      }
         
     
    /**
     *覆盖doEndTag方法
     */
    public int doEndTag() throws JspTagException 
    {
        try 
        {  
             if(bodyContent != null)  
             {
            	 if(items instanceof List){
            		 List lst=(List) items;
            		 for(Object o:lst){
            			 super.setValue("o", o);
            			 bodyContent.writeOut(bodyContent.getEnclosingWriter()); 
            		 }
            	 }
            	 
                 
             }
        } 
        catch(java.io.IOException e)
        {
            throw new JspTagException("IO Error: " + e.getMessage());  
        }   
        return EVAL_BODY_AGAIN ;  
    }

	public void setItems(Object items) {
		IbpdLogger.getLogger(this.getClass()).info(items);
		this.items = items;
	}

	public Object getItems() {
		return items;
	}
      
}