package com.ibpd.henuocms.service.SystemProper;

import com.ibpd.dao.impl.IBaseService;
import com.ibpd.henuocms.entity.SystemProperEntity;

public interface ISystemProperService extends IBaseService<SystemProperEntity> {
	void saveProperByKeyValue(String key,String value);
	SystemProperEntity getEntityByKey(String key);
}
 