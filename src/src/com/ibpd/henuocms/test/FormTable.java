package com.ibpd.henuocms.test;

import java.util.ArrayList;
import java.util.List;

public class FormTable {

	private String name;
	private String tableName;
	private List columnAttrList;
	private List<ColumnAttribute> formAttributeList=new ArrayList<ColumnAttribute>();
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public List<ColumnAttribute> getFormAttributeList() {
		return formAttributeList;
	}
	public void setFormAttributeList(List<ColumnAttribute> formAttributeList) {
		this.formAttributeList = formAttributeList;
	}
	
}
